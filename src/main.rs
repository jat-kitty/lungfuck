use clap::clap_app;

use lfc::lex::Lexer;
use lfc::prod::Producer;

fn main() {
	let matches = clap_app!(lungfuck =>
		(version: "0.1.0")
		(author: "Stefan 'jat' Wegert")
		(about: "a brainfuck compiler written in rust")
		(@arg INPUT: +required "the file to be compiled")
		(@arg OUTPUT: -o --output +takes_value "the path of the output file")
	)
	.get_matches();

	let input_file = matches.value_of("INPUT").unwrap().to_string();
	let _output_file = matches.value_of("OUTPUT").unwrap_or("a.wut").to_string();

	let mut lexer = Lexer::new(input_file).unwrap();
	let v = lexer.collect().unwrap();

	println!("{:?}", v);
}
