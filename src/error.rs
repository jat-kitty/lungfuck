use failure::Fail;

use std::convert::From;
use std::io;

#[derive(Debug, Fail)]
pub enum LungError {
	#[fail(display = "unexpected end of loop at {}; {}", _0, _1)]
	UnexpectedLoopEnd(usize, usize),

	#[fail(display = "a loop has started at {}; {}, but not ended", _0, _1)]
	MissingLoopEnd(usize, usize),

	#[fail(display = "the source provided is empty")]
	EmptySource,

	#[fail(
		display = "overflow at pos {}; {}, cell size is limited to 255",
		_0, _1
	)]
	CellOverflow(usize, usize),

	#[fail(display = "pointer out of bounds, buffer size is limited to 2000")]
	BufferOverflow,

	#[fail(display = "{}", _0)]
	IoError(io::Error),
}

impl From<io::Error> for LungError {
	fn from(e: io::Error) -> LungError {
		LungError::IoError(e)
	}
}
