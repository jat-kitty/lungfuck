/// Producers are similar to Iter, but simpler
pub trait Producer<T> {
	type Error;

	/// Spits out the next Item or None
	fn next(&mut self) -> Result<Option<T>, Self::Error>;
	/// Spits out all Items as a Vector
	fn collect(&mut self) -> Result<Vec<T>, Self::Error>;
	/// Spits out if the Producer has finished
	fn has_finished(&self) -> bool;
}
