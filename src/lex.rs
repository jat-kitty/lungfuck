use smallvec::{smallvec, SmallVec};

use std::fs;
use std::option::Option;

use crate::error::LungError;
use crate::prod::Producer;

#[derive(Clone, PartialEq, Debug)]
pub enum TokenType {
	Increase,
	Decrease,
	Left,
	Right,
	Dot,
	Comma,
	LoopBeginn,
	LoopEnd,
	EOF,
}

#[derive(Clone, PartialEq, Debug)]
pub struct Token {
	identifier: TokenType,
	position: [usize; 2],
}

impl Token {
	pub fn new(t: TokenType, pos: [usize; 2]) -> Token {
		Token {
			identifier: t,
			position: pos,
		}
	}
}

#[derive(Clone, PartialEq, Debug)]
pub struct Lexer {
	filename: String,
	src: Vec<u8>,
	finished: bool,
	loop_counter: usize,
	index: usize,
	position: [usize; 2],
}

impl Lexer {
	pub fn new(filename: String) -> Result<Lexer, SmallVec<[LungError; 1]>> {
		let s = fs::read_to_string(&filename).map_err(|e| smallvec![LungError::from(e)])?;

		if s.is_empty() {
			return Err(smallvec![LungError::EmptySource]);
		}

		Ok(Lexer {
			filename: filename,
			src: s.into_bytes(),
			finished: false,
			loop_counter: 0,
			index: 0,
			position: [0, 0],
		})
	}

	fn inc_index(&mut self) {
		self.position[1] += 1;
		self.index += 1;
		if self.index >= self.src.len() {
			self.finished = true;
		}
	}
}

impl Producer<Token> for Lexer {
	type Error = SmallVec<[LungError; 1]>;

	fn next(&mut self) -> Result<Option<Token>, Self::Error> {
		if self.finished == true {
			return Ok(None);
		}

		match self.src[self.index] {
			b'+' => {
				self.inc_index();

				Ok(Some(Token::new(TokenType::Increase, self.position)))
			}
			b'-' => {
				self.inc_index();
				Ok(Some(Token::new(TokenType::Decrease, self.position)))
			}
			b'>' => {
				self.inc_index();
				Ok(Some(Token::new(TokenType::Left, self.position)))
			}
			b'<' => {
				self.inc_index();
				Ok(Some(Token::new(TokenType::Right, self.position)))
			}
			b'.' => {
				self.inc_index();
				Ok(Some(Token::new(TokenType::Dot, self.position)))
			}
			b',' => {
				self.inc_index();
				Ok(Some(Token::new(TokenType::Comma, self.position)))
			}

			b'[' => {
				self.inc_index();

				self.loop_counter += 1;
				Ok(Some(Token::new(TokenType::LoopBeginn, self.position)))
			}

			b']' => {
				self.inc_index();

				if self.loop_counter == 0 {
					return Err(smallvec![LungError::UnexpectedLoopEnd(
						self.position[0],
						self.position[1]
					)]);
				}
				self.loop_counter -= 1;

				Ok(Some(Token::new(TokenType::LoopEnd, self.position)))
			}
			b'\n' => {
				self.inc_index();
				self.position[0] += 1;
				self.position[1] = 0;

				Ok(None)
			}
			_ => {
				self.inc_index();

				Ok(None)
			}
		}
	}

	fn collect(&mut self) -> Result<Vec<Token>, Self::Error> {
		let mut v = vec![];

		while self.finished == false {
			if let Some(item) = self.next()? {
				v.push(item);
			}
		}

		Ok(v)
	}
	fn has_finished(&self) -> bool {
		self.finished
	}
}
